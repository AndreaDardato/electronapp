const electron = require('electron');

const { app, BrowserWindow } = electron;

let mainWindow;

app.on('ready', () => {

    mainWindow = new BrowserWindow({
        width: 800,
        height: 800,
        resizable: true
    });

    mainWindow.loadURL("http://localhost:8081");

});
